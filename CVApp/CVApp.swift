//
//  AppDelegate.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct LoadingCVURLError: Error {}

@main
struct CVApp: App {
    @Environment(\.colorScheme) var colorScheme

    private var cvLoader: CVLoader = CVLoaderImpl()
    private var cv: CV!
    private var cvLoaded: Bool = false

    init() {
        do {
            try loadCV()
        } catch {
            self.cvLoaded = false
        }
    }

    var body: some Scene {
        WindowGroup {
            if self.cvLoaded {
                CVView(viewModel: cv)
                    .background(Color(UIColor.systemBackground).ignoresSafeArea())
            } else {
                ErrorLoadingCVView()
                    .padding()
            }
        }
    }

    private mutating func loadCV() throws {
        guard let url = Bundle.main.url(forResource: "cv", withExtension: "json") else {
            throw LoadingCVURLError()
        }

        self.cv = try CVLoaderImpl().load(from: url)
        self.cvLoaded = true
    }
}

struct ErrorLoadingCVView: View {
    var body: some View {
        Text("error loading cv".localized)
            .multilineTextAlignment(.center)
            .font(.title2)
            .animation(.easeInOut)
    }
}


