//
//  Constants.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation
import UIKit

final class Constant {
    static let linkAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.underlineColor: UIColor.blue,
                                                                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                                                NSAttributedString.Key.foregroundColor: UIColor.blue]

    struct Font {
        static let titleFontSize: CGFloat = 18.0
        static let primaryFontSize: CGFloat = 16.0
        static let secondaryFontSize: CGFloat = 12.0
    }

    struct Stack {
        static let stackSpacing: CGFloat = 8.0
    }
}
