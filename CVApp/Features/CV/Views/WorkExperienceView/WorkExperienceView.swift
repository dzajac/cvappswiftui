//
//  WorkExperienceView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 31/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct WorkExperienceView: View {
    @State var viewModel: WorkExperienceViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
            if let items = viewModel.workExperienceItems {
                ForEach(0..<items.count, id: \.self) {
                    let item = items[$0]
                    VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
                        HStack {
                            Text(item.company)
                                .font(.system(size: Constant.Font.secondaryFontSize, weight: .bold))
                                .frame(alignment: .leading)

                            Spacer()

                            Text(item.timePeriod)
                                .font(Font.system(size: Constant.Font.secondaryFontSize, weight: .light).italic())
                                .frame(alignment: .trailing)
                        }

                        Text(item.role)
                            .font(.system(size: Constant.Font.secondaryFontSize, weight: .bold))

                        ForEach(item.responsibilities, id: \.self) { responsibility in
                            BulletPoint(viewModel: BulletPointViewModel(text: responsibility.description))
                        }
                    }
                }
                .padding(.bottom, Constant.Stack.stackSpacing * 2)
            }
        }
    }
}
