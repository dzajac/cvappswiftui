//
//  SkillsView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct SkillsView: View {
    @State var viewModel: SkillsViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
            ForEach(viewModel.skills, id: \.self) { skill in
                BulletPoint(viewModel: BulletPointViewModel(text: skill.description))
            }
        }
    }
}
