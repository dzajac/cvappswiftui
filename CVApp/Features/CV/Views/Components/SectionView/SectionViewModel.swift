//
//  SectionViewModel.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI
import Combine
import Foundation

class SectionViewModel: ObservableObject {
    @Published var sectionTitle: String

    init(title: String) {
        self.sectionTitle = title
    }
}
