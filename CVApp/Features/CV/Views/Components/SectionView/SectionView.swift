//
//  SectionView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI
import UIKit

struct Section: View {
    private enum SectionConstant {
        static let spacing: CGFloat = 4.0
        static let dividerHeight: CGFloat = 2.0
        static let topPadding: CGFloat = 8.0
        static let animationDelay: TimeInterval = 0.1
    }

    @State var viewModel: SectionViewModel
    @State private(set) var container: SwiftUI.AnyView
    @State private var expanded: Bool = true

    @State private var scale: CGFloat = 1

    var body: some View {
        VStack(alignment: .leading, spacing: SectionConstant.spacing) {
            HStack {
                Text(viewModel.sectionTitle)
                    .font(.system(size: Constant.Font.titleFontSize, weight: .bold))
                Spacer()
                ExpandButton(expanded: $expanded, width: 2)
                    .frame(width: 10, height: 10, alignment: .center)
                    .padding(.trailing, 8)
                    .rotation3DEffect(
                        expanded ? Angle(degrees: 0) : Angle(degrees: 180),
                        axis: (x: 1.0, y: 0.0, z: 0.0),
                        anchor: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/,
                        anchorZ: 1,
                        perspective: 1.0
                    )
            }
            .contentShape(Rectangle())
            .onTapGesture {
                withAnimation {
                    self.expanded.toggle()
                }
                withAnimation(
                    Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                        .delay(SectionConstant.animationDelay)) {
                    self.scale = expanded ? 1 : 0.7
                }
            }
            Rectangle()
                .background(Color.black)
                .frame(height: SectionConstant.dividerHeight)

            if expanded {
                container
                    .padding(.top, SectionConstant.topPadding)
                    .fixedSize(horizontal: false, vertical: true)
                    .scaleEffect(scale, anchor: .topLeading)
            }
        }
    }
}
