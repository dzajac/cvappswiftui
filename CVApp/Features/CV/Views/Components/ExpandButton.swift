//
//  ExpandButton.swift
//  CVApp
//
//  Created by dzajac on 07/06/2021.
//  Copyright © 2021 Dariusz Zajac. All rights reserved.
//

import SwiftUI
import Combine

struct ExpandButton: View {
    @Environment(\.colorScheme) var colorScheme

    @Binding var expanded: Bool
    @State var color: Color = Color.gray

    var width: CGFloat = 3.0

    var body: some View {
        GeometryReader { g in
            Line(
                start: CGPoint(x: g.frame(in: .local).origin.x, y: g.size.height / 2),
                end: CGPoint(x: g.size.width / 2, y: g.frame(in: .local).origin.y)
            )
            .stroke(color, style: StrokeStyle(lineWidth: width, lineCap: .round, lineJoin: .round))

            Line(
                start: CGPoint(x: g.size.width / 2, y: g.frame(in: .local).origin.y),
                end: CGPoint(x: g.size.width, y: g.size.height / 2)
            )
            .stroke(color, style: StrokeStyle(lineWidth: width, lineCap: .round, lineJoin: .round))
        }
        .onTapGesture {
            withAnimation {
                expanded.toggle()
            }
        }
    }
}

struct Line: Shape {
    @State var start: CGPoint
    @State var end: CGPoint

    var animatableData: AnimatablePair<CGPoint.AnimatableData, CGPoint.AnimatableData> {
        get { AnimatablePair(start.animatableData, end.animatableData) }
        set { (start.animatableData, end.animatableData) = (newValue.first, newValue.second) }
    }

    func path(in rect: CGRect) -> Path {
        Path { p in
            p.move(to: start)
            p.addLine(to: end)
        }
    }
}

struct ExpandButton_Previews: PreviewProvider {
    @State static var expanded: Bool = true
    static var previews: some View {
        ExpandButton(expanded: $expanded)
            .frame(width: 20, height: 20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .onTapGesture {
                expanded.toggle()
            }
    }
}
