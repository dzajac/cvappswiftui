//
//  BulletPointView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct BulletPoint: View {
    private enum BulletPointConstant {
        static let bulletSize: CGFloat = 5.0
        static let bulletAlignmentAdjustment: CGFloat = -4.0
    }
    @State var viewModel: BulletPointViewModel

    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top, spacing: Constant.Stack.stackSpacing) {
                Circle()
                    .frame(width: BulletPointConstant.bulletSize, height: BulletPointConstant.bulletSize, alignment: .center)
                    .alignmentGuide(.top) { dimension -> CGFloat in
                        dimension[.top] + BulletPointConstant.bulletAlignmentAdjustment
                    }
                    .background(Color.black)

                Text(viewModel.text)
                    .font(.system(size: Constant.Font.secondaryFontSize))
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.leading)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
    }
}
