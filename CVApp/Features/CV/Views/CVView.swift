//
//  View.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI
import Combine

struct CVView: View {
    private enum CVViewConstant {
        static let stackSpacking: CGFloat = 25
        static let horizontalPadding: CGFloat = 20
        static let verticalPadding: CGFloat = 10
        static let animationStartDelay: Double = 1.0
    }

    @State var loaded: Bool = false
    @State var viewModel: CV
    @State var objectiveOffsetAnimate: Bool = false
    @State var educationOffsetAnimate: Bool = false
    @State var technicalOffsetAnimate: Bool = false
    @State var experienceOffsetAnimate: Bool = false
    @State var skillsOffsetAnimate: Bool = false

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .leading, spacing: CVViewConstant.stackSpacking) {
                ContactInfoView(animate: $loaded, viewModel: ContactInfoViewModel(with: viewModel.contactInfo))
                    .padding(.top, CVViewConstant.verticalPadding)

                Section(
                    viewModel: SectionViewModel(title: "objective".localized),
                    container: AnyView(
                        Text(viewModel.objective)
                            .font(.system(size: Constant.Font.secondaryFontSize))
                    )
                )
                .offset(y: objectiveOffsetAnimate ? 0 : 30)
                .opacity(loaded ? 1 : 0)

                Section(
                    viewModel: SectionViewModel(title: "education".localized),
                    container: AnyView(
                        EducationView(
                            viewModel: EducationViewModel(
                                program: viewModel.education.program,
                                timePeriod: viewModel.education.timePeriod,
                                school: viewModel.education.school)
                        )
                    )
                )
                .offset(y: educationOffsetAnimate ? 0 : 30)
                .opacity(educationOffsetAnimate ? 1 : 0)

                Section(
                    viewModel: SectionViewModel(title: "technical proficiency".localized),
                    container: AnyView(
                        TechnicalProficiencyView(viewModel: TechnicalProficiencyViewModel(proficiencies: viewModel.technicalProficiency))
                    )
                )
                .offset(y: technicalOffsetAnimate ? 0 : 30)
                .opacity(technicalOffsetAnimate ? 1 : 0)

                Section(
                    viewModel: SectionViewModel(title: "work experience".localized),
                    container: AnyView(
                        WorkExperienceView(viewModel: WorkExperienceViewModel(workExperienceItems: viewModel.workExperience))
                    )
                )
                .offset(y: experienceOffsetAnimate ? 0 : 30)
                .opacity(experienceOffsetAnimate ? 1 : 0)

                if let skills = viewModel.skills {
                    Section(
                        viewModel: SectionViewModel(title: "skills".localized),
                        container: AnyView(SkillsView(viewModel: SkillsViewModel(skills: skills)))
                    )
                    .offset(y: skillsOffsetAnimate ? 0 : 30)
                    .opacity(skillsOffsetAnimate ? 1 : 0)
                }
            }
            .fixedSize(horizontal: false, vertical: true)
        }
        .padding(.horizontal, CVViewConstant.horizontalPadding)
        .padding(.vertical, CVViewConstant.verticalPadding)
        .onAppear {
            withAnimation(Animation.easeInOut.delay(CVViewConstant.animationStartDelay)) {
                loaded.toggle()
            }
            withAnimation(Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                            .delay(CVViewConstant.animationStartDelay)) {
                objectiveOffsetAnimate.toggle()
            }
            withAnimation(Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                            .delay(CVViewConstant.animationStartDelay + 0.2)) {
                educationOffsetAnimate.toggle()
            }
            withAnimation(Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                            .delay(CVViewConstant.animationStartDelay + 0.4)) {
                technicalOffsetAnimate.toggle()
            }
            withAnimation(Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                .delay(CVViewConstant.animationStartDelay + 0.6)) {
                experienceOffsetAnimate.toggle()
            }
            withAnimation(Animation.spring(response: 0.3, dampingFraction: 0.4, blendDuration: 1)
                .delay(CVViewConstant.animationStartDelay + 0.8)) {
                skillsOffsetAnimate.toggle()
            }
        }
    }
}
