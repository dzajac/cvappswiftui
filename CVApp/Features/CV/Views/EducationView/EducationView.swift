//
//  EducationView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct EducationView: View {
    @State var viewModel: EducationViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
            Text(viewModel.timePeriod)
                .font(Font.system(size: Constant.Font.secondaryFontSize).italic())
            Text(viewModel.program)
                .font(.system(size: Constant.Font.secondaryFontSize))
            Text(viewModel.school)
                .font(.system(size: Constant.Font.secondaryFontSize))
        }
    }
}
