//
//  TechnicalProficiencyView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct TechnicalProficiencyView: View {
    @State var viewModel: TechnicalProficiencyViewModel

    var body: some View {
        VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
            ForEach(0..<viewModel.proficiencies.count, id:\.self) {
                let proficiency = viewModel.proficiencies[$0]
                VStack(alignment: .leading, spacing: Constant.Stack.stackSpacing) {
                    Text(proficiency.title)
                        .font(.system(size: Constant.Font.titleFontSize, weight: .bold))
                    Text(proficiency.description)
                        .font(.system(size: Constant.Font.secondaryFontSize))
                }
            }
        }
    }
}
