//
//  ContactInfoViewModel.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI
import Combine

final class ContactInfoViewModel: ObservableObject {
    private(set) var contactInfo: ContactInfo

    init(with contactInfo: ContactInfo) {
        self.contactInfo = contactInfo
    }
}
