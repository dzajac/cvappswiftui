//
//  ContactInfoView.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import SwiftUI

struct ContactInfoView: View {
    private enum ContactInfoViewConstant {
        static let dividerPadding: CGFloat = 4
        static let dividerHeight: CGFloat = 15
        static let animationDelay: Double = 0.3
    }
    @Binding var animate: Bool
    @State var viewModel: ContactInfoViewModel

    var body: some View {
        VStack(spacing: Constant.Stack.stackSpacing) {
            HStack {
                GeometryReader { geometry in
                    Text(viewModel.contactInfo.fullName)
                        .font(.system(size: Constant.Font.titleFontSize, weight: .bold))
                        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading)
                        .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay))
                        .offset(x: self.animate ? 0 : -geometry.size.width)
                }

                Spacer()

                GeometryReader { geometry in
                    Text(viewModel.contactInfo.street)
                        .font(.system(size: Constant.Font.secondaryFontSize))
                        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .trailing)
                        .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay))
                        .offset(x: self.animate ? 0 : geometry.size.width)
                }
            }

            HStack {
                GeometryReader { geometry in
                    Text(viewModel.contactInfo.email)
                        .font(.system(size: Constant.Font.secondaryFontSize))
                        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading)
                        .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay * 1.5))
                        .offset(x: self.animate ? 0 : -geometry.size.width)
                }

                Spacer()

                GeometryReader { geometry in
                    Text(viewModel.contactInfo.postCode)
                        .font(.system(size: Constant.Font.secondaryFontSize))
                        .frame(width: geometry.size.width, height: geometry.size.height, alignment: .trailing)
                        .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay * 1.5))
                        .offset(x: self.animate ? 0 : geometry.size.width)
                }
            }

            HStack {
                if let phone = viewModel.contactInfo.phone {
                    GeometryReader { geometry in
                        Text(phone)
                            .font(.system(size: Constant.Font.secondaryFontSize))
                            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .leading)
                            .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay * 2))
                            .offset(x: self.animate ? 0 : -geometry.size.width)
                    }
                    Spacer()
                }

                
                GeometryReader { geometry in
                    Text("\(viewModel.contactInfo.city), \(viewModel.contactInfo.country)")
                        .font(.system(size: Constant.Font.secondaryFontSize))
                        .frame(width: geometry.size.width, height: geometry.size.height,
                               alignment: .trailing)
                        .animation(Animation.easeInOut.delay(ContactInfoViewConstant.animationDelay * 2))
                        .offset(x: self.animate ? 0 : geometry.size.width)
                }
            }

            GeometryReader { geometry in
                Rectangle()
                    .frame(width: geometry.size.width, height: ContactInfoViewConstant.dividerHeight)
                    .background(Color.black)
                    .cornerRadius(7, corners: [.bottomLeft, .topRight])
                    .padding(.top, ContactInfoViewConstant.dividerPadding)
                    .opacity(self.animate ? 1 : 0)
                    .animation(
                        Animation.easeIn(duration: 0.6)
                            .delay(ContactInfoViewConstant.animationDelay * 2.5)
                    )
            }
        }
    }
}
