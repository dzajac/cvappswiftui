//
//  CV.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

struct CV: Codable {
    var contactInfo: ContactInfo
    var objective: String
    var technicalProficiency: [TechnicalProficiency]
    var skills: [String]?
    var education: Education
    var workExperience: [WorkExperienceItem]?
}
