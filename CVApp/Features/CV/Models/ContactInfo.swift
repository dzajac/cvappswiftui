//
//  ContactInfo.swift
//  CVApp
//
//  Created by Dariusz Zajac on 30/05/2020.
//  Copyright © 2020 Dariusz Zajac. All rights reserved.
//

import Foundation

struct ContactInfo: Codable {
    var fullName: String
    var email: String
    var phone: String?
    var street: String
    var postCode: String
    var city: String
    var country: String
    
    enum CodingKeys: String, CodingKey {
        case fullName = "name"
        case email, phone, street, postCode, city, country
    }
}
